# 2019 Conference

This repository provides resources associated with the 
[2019 AAPOR conference](https://www.aapor.org/Conference-Events/Annual-Meeting.aspx).
